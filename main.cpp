#include "Dominios.h"


#include <iostream>
#include <stdio.h>
#include <string>
#include <unistd.h>
#include <iomanip>

using namespace std;

void imprimir(Moto *Origem, ETB *Etb_a){        //Imprimir as informações necessarias.
    cout << "Motorcycle plate: " << Origem->get_plate() << endl;
    cout << "Speed: " << Origem->get_speed() << endl;
    
    if(Origem->battery->estado == 0){
        cout << "Attached battery UID: NONE" << endl;
    }
    if(Origem->battery->estado == 1){
        cout << "Attached battery UID: " << Origem->battery->get_uid() << endl;
    }

    if(Origem->battery->estado == 0){
        cout << "Motorcycle battery SoC: NONE" << endl;
    }
    if(Origem->battery->estado == 1){
        cout << "Motorcycle battery SoC: " << Origem->battery->get_soc() << "\n" << endl;
    }
    
    cout << "ETB ID: " << Etb_a->get_uid() << endl;
    if(Etb_a->CP1->battery->estado == 0){
        cout << "CP 1: [NONE | NONE | NO]" << endl;
    }
    if(Etb_a->CP1->battery->estado == 1){
        cout << "CP 1: [" << Etb_a->CP1->battery->get_uid() << " | " << Etb_a->CP1->battery->get_soc() << "% | charging: ";
        if(Etb_a->CP1->estado == 0)
            cout << "NO" << endl;
        if(Etb_a->CP1->estado == 1)
            cout << "YES" << endl;
    }
    if(Etb_a->CP2->battery->estado == 1){
        cout << "CP 2: [" << Etb_a->CP2->battery->get_uid() << " | " << Etb_a->CP2->battery->get_soc() << "% | charging: ";
        if(Etb_a->CP2->estado == 0)
            cout << "NO" << endl;
        if(Etb_a->CP2->estado == 1)
            cout << "YES" << endl;
    }
    if(Etb_a->CP3->battery->estado == 1){
        cout << "CP 3: [" << Etb_a->CP3->battery->get_uid() << " | " << Etb_a->CP3->battery->get_soc() << "% | charging: ";
        if(Etb_a->CP3->estado == 0)
            cout << "NO" << endl;
        if(Etb_a->CP3->estado == 1)
            cout << "YES" << endl;
    }
    if(Etb_a->CP4->battery->estado == 1){
        cout << "CP 4: [" << Etb_a->CP4->battery->get_uid() << " | " << Etb_a->CP4->battery->get_soc() << "% | charging: ";
        if(Etb_a->CP4->estado == 0)
            cout << "NO" << endl;
        if(Etb_a->CP4->estado == 1)
            cout << "YES" << endl;
    }
    if(Etb_a->CP5->battery->estado == 1){
        cout << "CP 5: [" << Etb_a->CP5->battery->get_uid() << " | " << Etb_a->CP5->battery->get_soc() << "% | charging: ";
        if(Etb_a->CP5->estado == 0)
            cout << "NO" << endl;
        if(Etb_a->CP5->estado == 1)
            cout << "YES" << endl;
    }
    if(Etb_a->CP6->battery->estado == 1){
        cout << "CP 6: [" << Etb_a->CP6->battery->get_uid() << " | " << Etb_a->CP6->battery->get_soc() << "% | charging: ";
        if(Etb_a->CP6->estado == 0)
            cout << "NO" << endl;
        if(Etb_a->CP6->estado == 1)
            cout << "YES" << endl;
    }
    if(Etb_a->CP7->battery->estado == 1){
        cout << "CP 7: [" << Etb_a->CP7->battery->get_uid() << " | " << Etb_a->CP7->battery->get_soc() << "% | charging: ";
        if(Etb_a->CP7->estado == 0)
            cout << "NO" << endl;
        if(Etb_a->CP7->estado == 1)
            cout << "YES" << endl;
    }
    cout << "\n\n" << endl;
}



int main(){
    int time = 0;                   //Tempo em seg da simulação
    int i = 0;
    Moto *Origem;
    Origem = new Moto();            //Cria um objeto moto com a bateria em 85% desligada
    ETB *Etb_a;
    Etb_a = new ETB();              //Cria um objeto ETB com as baterias conforme passado

    Origem->ligar();

    cout << setprecision(2) << fixed;


    while(time < 1140){             //Os 6 ciclos iniciais
        for (i = 0; i < 180; i++){
            Origem->acionar_acel();
            Origem->descarregar_moto_ligada();
            Etb_a->carregar_bateria(1);
            Etb_a->carregar_bateria(2);
            Etb_a->carregar_bateria(3);
            Etb_a->carregar_bateria(4);
            Etb_a->carregar_bateria(5);
            Etb_a->carregar_bateria(6); 
            Etb_a->carregar_bateria(7);
            time++;
            if(time % 10 == 0){
                imprimir(Origem, Etb_a);
                 
            }      
        }
        for (i = 0; i < 10; i++){
            Origem->acionar_freio();
            Origem->descarregar_moto_ligada();
            Etb_a->carregar_bateria(1);
            Etb_a->carregar_bateria(2);
            Etb_a->carregar_bateria(3);
            Etb_a->carregar_bateria(4);
            Etb_a->carregar_bateria(5);
            Etb_a->carregar_bateria(6); 
            Etb_a->carregar_bateria(7);
            time++;
            if(time % 10 == 0){
                imprimir(Origem, Etb_a);
                 
            }  
        }
    }

    
    while(time < 528){             //Os 4 ciclos intermediarios
        for (i = 0; i < 120; i++){
            Origem->acionar_acel();
            Origem->descarregar_moto_ligada();
            Etb_a->carregar_bateria(1);
            Etb_a->carregar_bateria(2);
            Etb_a->carregar_bateria(3);
            Etb_a->carregar_bateria(4);
            Etb_a->carregar_bateria(5);
            Etb_a->carregar_bateria(6); 
            Etb_a->carregar_bateria(7);
            time++;
            if(time % 10 == 0){
                imprimir(Origem, Etb_a);
                 
            }  
        }
        for (i = 0; i < 12; i++){
            Origem->acionar_freio();
            Origem->descarregar_moto_ligada();
            Etb_a->carregar_bateria(1);
            Etb_a->carregar_bateria(2);
            Etb_a->carregar_bateria(3);
            Etb_a->carregar_bateria(4);
            Etb_a->carregar_bateria(5);
            Etb_a->carregar_bateria(6); 
            Etb_a->carregar_bateria(7);
            time++;
            if(time % 10 == 0){
                imprimir(Origem, Etb_a);
                 
            }  
        }
    }


    for (i = 0; i < 100; i++){          //Acelera por 1min e 40seg
        Origem->acionar_acel();
        Origem->descarregar_moto_ligada();
        Etb_a->carregar_bateria(1);
        Etb_a->carregar_bateria(2);
        Etb_a->carregar_bateria(3);
        Etb_a->carregar_bateria(4);
        Etb_a->carregar_bateria(5);
        Etb_a->carregar_bateria(6); 
        Etb_a->carregar_bateria(7);
        time++;
        if(time % 10 == 0){
            imprimir(Origem, Etb_a);
             
        }  
    }

    for (i = 0; i < 32; i++){           //Freia por 32seg
        Origem->acionar_freio();
        Origem->descarregar_moto_ligada();
        Etb_a->carregar_bateria(1);
        Etb_a->carregar_bateria(2);
        Etb_a->carregar_bateria(3);
        Etb_a->carregar_bateria(4);
        Etb_a->carregar_bateria(5);
        Etb_a->carregar_bateria(6); 
        Etb_a->carregar_bateria(7);
        time++;
        if(time % 10 == 0){
            imprimir(Origem, Etb_a);
             
        }  
    }

    Bateria bat_1(0, 0, 0, 0);                      //Cria um objeto bateria com uid 0, soc 0, host 0 e no estado Idle                                 
    Bateria bat_2(0, 0, 0, 0);

    bat_1 = Origem->liberar_bateria(bat_1);         //Bateria removida do moto
    bat_2 = Etb_a->liberar_bateria(1, bat_2);       //Bateria removida da ETB na CP numero 1
    
    Etb_a->associar_bateria(7, bat_1);              //Bateria associada a ETB na CP numero 7


    cout << "\n\n\nBATERIA REIRADA DA MOTO E INSERIDA NO LOCAL VAGO DA ETB\n\n" << endl;

    imprimir(Origem, Etb_a);
     

    time = time + 10;
    
    Origem->associar_bateria(bat_2);                //Bateria associada à moto
    Origem->desligar();

    cout << "\n\n\nNOVA BATERIA INSERIDA NA MOTO\n\n" << endl;

    imprimir(Origem, Etb_a);
     


    for (i = 0; i < 60; i++){                       //Moto mantida parada por 60seg após chegar na ETB
        Origem->descarregar_moto_desligada();
        Etb_a->carregar_bateria(1);
        Etb_a->carregar_bateria(2);
        Etb_a->carregar_bateria(3);
        Etb_a->carregar_bateria(4);
        Etb_a->carregar_bateria(5);
        Etb_a->carregar_bateria(6); 
        Etb_a->carregar_bateria(7);
        time++;
        if(time % 10 == 0){
            imprimir(Origem, Etb_a);
             
        }  
    }    

    return 0;
}