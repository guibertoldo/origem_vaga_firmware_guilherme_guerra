#ifndef DOMINIOS_H
#define DOMINIOS_H

#include <iostream>
#include <stdio.h>
#include <string>


using namespace std;
// Forward declarations.

class Moto;
class Bateria;
class ETB;
class CP;


class Bateria {
    public:
        Bateria(long long int, float, bool, bool);

        void associar_host(bool);
        void disassociar_host();
        long long int get_uid(){
            return uid;
        }
        float get_soc(){
            return soc;
        }
        bool get_host(){
            return host;
        }
        void set_uid(long long int);
        void set_soc(float);
        void set_host(bool);

        bool estado;    // 0 = Idle | 1 = Attached
    
    private:
        long long int uid;          
        float soc;
        bool host;      // 0 = Moto | 1 = ETB

};

class Moto {
    public:
        Moto();
        
        void ligar();
        void associar_bateria(Bateria);
        Bateria liberar_bateria(Bateria);
        void acionar_acel();
        void acionar_freio();
        void desligar();
        string get_plate(){
            return plate;
        }
        float get_speed(){
            return speed;
        }
        long long int get_bateria_uid(){
            return battery->get_uid();
        }
        float get_bateria_soc(){
            return battery->get_soc();
        }
        bool get_bateria_host(){
            return battery->get_host();
        }
        void set_plate(string);
        void set_speed(float);

        void descarregar_moto_ligada();         //Metodo para reduzir o soc da bateria enquanto a moto estiver ligada
        void descarregar_moto_desligada();      //Metodo para reduzir o soc da bateria enquanto a moto estiver desligada|standby 
        bool estado;        // 0 = Standby | 1 = On
        Bateria *battery;
 
    private:
        string plate;
        float speed;       
};

class CP{
    public:
        CP(long long int, float, int);
        bool estado;    // 0 = Idle | 1 = Charging

        Bateria *battery;
};

class ETB {
    public:
        ETB();
        
        void associar_bateria(int, Bateria);
        void carregar_bateria(int);
        void desligar_carregamento(int);
        Bateria liberar_bateria(int, Bateria);
        int baterias_presentes();
        int baterias_carregando();
        float tempo_carregamento(int);
        long int get_uid(){
            return uid;
        }
        bool get_cp1(){
            return CP1->estado;
        }
        bool get_cp2(){
            return CP2->estado;
        }
        bool get_cp3(){
            return CP3->estado;
        }
        bool get_cp4(){
            return CP4->estado;
        }
        bool get_cp5(){
            return CP5->estado;
        }
        bool get_cp6(){
            return CP6->estado;
        }
        bool get_cp7(){
            return CP7->estado;
        }
        CP *CP1;
        CP *CP2;
        CP *CP3;
        CP *CP4;
        CP *CP5;
        CP *CP6;
        CP *CP7;
 
    private:
        long int uid;
        
};





#endif