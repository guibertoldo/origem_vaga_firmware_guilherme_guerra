#include <iostream>
#include <stdio.h>
#include <string>

#include "Dominios.h"

#define MAX_SPEED 60.0
using namespace std;


Moto::Moto(){                                       //Construtor da classe MOTO com os parametros informados
    this->estado = 0;
    this->plate = "ORI4321";
    this->speed = 0.0;
    battery = new Bateria(9999, 85.0, 0, 1);
}

void Moto::ligar(){
    if(battery->estado == 1){
        this->estado = 1;
    }  
}

void Moto::associar_bateria(Bateria bat){
    battery->set_host(0);
    battery->set_soc(bat.get_soc());
    battery->set_uid(bat.get_uid());
    battery->estado = 1;
}

Bateria Moto::liberar_bateria(Bateria bat){
    bat.set_soc(battery->get_soc());
    bat.set_uid(battery->get_uid());
    bat.estado = 0;

    battery->estado = 0;
    battery->set_soc(0);
    battery->set_uid(0);

    return bat;
}

void Moto::acionar_acel(){
    if(battery->estado == 1){                       //Testa se tem alguma bateria associada a moto antes de acelerar
        speed = this->get_speed() + 0.2;
        this->set_speed(speed);
        if(this->get_speed() > MAX_SPEED){
            this->set_speed(MAX_SPEED);
        }
    }  
}

void Moto::acionar_freio(){
    if(battery->estado == 1){           
        speed = this->get_speed() - 2.0;
        this->set_speed(speed);
        if(this->get_speed() < 0.0){
            this->set_speed(0.0);
        }
    } 
}

void Moto::desligar(){
    this->estado = 0;
}

void Moto::set_plate(string moto_plate){
    this->plate = moto_plate;
}

void Moto::set_speed(float moto_speed){
    this->speed = moto_speed;
}

void Moto::descarregar_moto_ligada(){           //Metodo criado para representar a perda de craga da bateria quando a moto ligada
    if(this->battery->get_soc() >= 0.0 && this->battery->get_soc() <= 100.0){
        float soc = this->battery->get_soc() - 0.01 - ((this->get_speed() / MAX_SPEED) * (this->get_speed() / MAX_SPEED)) * 0.05;
        this->battery->set_soc(soc);
    }
    if(this->battery->get_soc() <= 0.0){
        this->battery->set_soc(0.0);
    }
    if(this->battery->get_soc() >= 100.0){
        this->battery->set_soc(100.0);
    }
}

void Moto::descarregar_moto_desligada(){        //Metodo criado para representar a perda de craga da bateria quando a moto desligada
    if(this->battery->get_soc() >= 0.0 && this->battery->get_soc() <= 100.0){
        float soc = this->battery->get_soc() - 0.01;
        this->battery->set_soc(soc);
    }
    if(this->battery->get_soc() < 0.0){
        this->battery->set_soc(0.0);
    }
    if(this->battery->get_soc() > 100.0){
        this->battery->set_soc(100.0);
    }
}

Bateria::Bateria(long long int bat_uid, float bat_soc, bool bat_host, bool bat_estado){
    this->uid = bat_uid;
    this->soc = bat_soc;
    this->host = bat_host;
    this->estado = bat_estado;
}

void Bateria::associar_host(bool bat_host){
    this->estado = 1;
    this->host = bat_host;
}

void Bateria::disassociar_host(){
    this->estado = 0;
}

void Bateria::set_uid(long long int bat_uid){      
    this->uid = bat_uid;
}

void Bateria::set_soc(float bat_soc){
    this->soc = bat_soc;
}

void Bateria::set_host(bool bat_host){
    this->host = bat_host;
}

ETB::ETB(){                                         //Construtor da classe ETB com os parametros informados
    this->uid = 10000;
    CP1 = new CP(1, 100.0, 1);
    CP2 = new CP(2, 100.0, 1);
    CP3 = new CP(3, 100.0, 1);
    CP4 = new CP(4, 70.0, 1);
    CP5 = new CP(5, 60.0, 1);
    CP6 = new CP(6, 50.0, 1);
    CP7 = new CP(0, 0.0, 0);
}

void ETB::associar_bateria(int CP_number, Bateria bat){
    if(CP_number == 1) {
        CP1->battery->set_host(1);
        CP1->battery->set_soc(bat.get_soc());
        CP1->battery->set_uid(bat.get_uid());
        CP1->battery->estado = 1;
    }
    if(CP_number == 2) {
        CP2->battery->set_host(1);
        CP2->battery->set_soc(bat.get_soc());
        CP2->battery->set_uid(bat.get_uid());
        CP2->battery->estado = 1;
    }
    if(CP_number == 3) {
        CP3->battery->set_host(1);
        CP3->battery->set_soc(bat.get_soc());
        CP3->battery->set_uid(bat.get_uid());
        CP3->battery->estado = 1;
    }
    if(CP_number == 4) {
        CP4->battery->set_host(1);
        CP4->battery->set_soc(bat.get_soc());
        CP4->battery->set_uid(bat.get_uid());
        CP4->battery->estado = 1;
    }
    if(CP_number == 5) {
        CP5->battery->set_host(1);
        CP5->battery->set_soc(bat.get_soc());
        CP5->battery->set_uid(bat.get_uid());
        CP5->battery->estado = 1;
    }
    if(CP_number == 6) {
        CP6->battery->set_host(1);
        CP6->battery->set_soc(bat.get_soc());
        CP6->battery->set_uid(bat.get_uid());
        CP6->battery->estado = 1;
    }
    if(CP_number == 7) {
        CP7->battery->set_host(1);
        CP7->battery->set_soc(bat.get_soc());
        CP7->battery->set_uid(bat.get_uid());
        CP7->battery->estado = 1;
    }
}

void ETB::carregar_bateria(int CP_number){
    if(CP_number == 1){
        if(CP1->battery->estado == 1){
            if(CP1->battery->get_soc() >= 0.0 && CP1->battery->get_soc() <= 100.0){
                float soc = CP1->battery->get_soc() + 0.05;
                CP1->battery->set_soc(soc);
                CP1->estado = 1;
            }
            if(CP1->battery->get_soc() > 100.0){     //Bateria carregada
                CP1->battery->set_soc(100.0);
                this->desligar_carregamento(1);
            }
        }
    }
    if(CP_number == 2){
        if(CP2->battery->estado == 1){
            if(CP2->battery->get_soc() >= 0.0 && CP2->battery->get_soc() <= 100.0){
                float soc = CP2->battery->get_soc() + 0.05;
                CP2->battery->set_soc(soc);
                CP2->estado = 1;
            }
            if(CP2->battery->get_soc() > 100.0){     
                CP2->battery->set_soc(100.0);
                this->desligar_carregamento(2);
            }
        }
    }
    if(CP_number == 3){
        if(CP3->battery->estado == 1){
            if(CP3->battery->get_soc() >= 0.0 && CP3->battery->get_soc() <= 100.0){
                float soc = CP3->battery->get_soc() + 0.05;
                CP3->battery->set_soc(soc);
                CP3->estado = 1;
            }
            if(CP3->battery->get_soc() > 100.0){     
                CP3->battery->set_soc(100.0);
                this->desligar_carregamento(3);
            }
        }
    }
    if(CP_number == 4){
        if(CP4->battery->estado == 1){
            if(CP4->battery->get_soc() >= 0.0 && CP4->battery->get_soc() <= 100.0){
                float soc = CP4->battery->get_soc() + 0.05;
                CP4->battery->set_soc(soc);
                CP4->estado = 1;
            }
            if(CP4->battery->get_soc() > 100.0){    
                CP4->battery->set_soc(100.0);
                this->desligar_carregamento(4);
            }
        }
    }
    if(CP_number == 5){
        if(CP5->battery->estado == 1){
            if(CP5->battery->get_soc() >= 0.0 && CP5->battery->get_soc() <= 100.0){
                float soc = CP5->battery->get_soc() + 0.05;
                CP5->battery->set_soc(soc);
                CP5->estado = 1;
            }
            if(CP5->battery->get_soc() > 100.0){     
                CP5->battery->set_soc(100.0);
                this->desligar_carregamento(5);
            }
        }
    }
    if(CP_number == 6){
        if(CP6->battery->estado == 1){
            if(CP6->battery->get_soc() >= 0.0 && CP6->battery->get_soc() <= 100.0){
                float soc = CP6->battery->get_soc() + 0.05;
                CP6->battery->set_soc(soc);
                CP6->estado = 1;
            }
            if(CP6->battery->get_soc() > 100.0){    
                CP6->battery->set_soc(100.0);
                this->desligar_carregamento(6);
            }
        }
    }
    if(CP_number == 7){
        if(CP7->battery->estado == 1){
            if(CP7->battery->get_soc() >= 0.0 && CP7->battery->get_soc() <= 100.0){
                float soc = CP7->battery->get_soc() + 0.05;
                CP7->battery->set_soc(soc);
                CP7->estado = 1;
            }
            if(CP7->battery->get_soc() > 100.0){   
                CP7->battery->set_soc(100.0);
                this->desligar_carregamento(7);
            }
        }
    }
}

void ETB::desligar_carregamento(int CP_number){
    if(CP_number == 1){
        CP1->estado = 0;
    }
    if(CP_number == 2){
        CP2->estado = 0;
    }
    if(CP_number == 3){
        CP3->estado = 0;
    }
    if(CP_number == 4){
        CP4->estado = 0;
    }
    if(CP_number == 5){
        CP5->estado = 0;
    }
    if(CP_number == 6){
        CP6->estado = 0;
    }
    if(CP_number == 7){
        CP7->estado = 0;
    }
}

Bateria ETB::liberar_bateria(int CP_number, Bateria bat){
    if(CP_number == 1){
        bat.set_soc(CP1->battery->get_soc());
        bat.set_uid(CP1->battery->get_uid());
        bat.estado = 0;

        CP1->battery->estado = 0;
        CP1->battery->set_soc(0);
        CP1->battery->set_uid(0);
    }
    if(CP_number == 2){
        bat.set_soc(CP2->battery->get_soc());
        bat.set_uid(CP2->battery->get_uid());
        bat.estado = 0;

        CP2->battery->estado = 0;
        CP2->battery->set_soc(0);
        CP2->battery->set_uid(0);
    }
    if(CP_number == 3){
        bat.set_soc(CP3->battery->get_soc());
        bat.set_uid(CP3->battery->get_uid());
        bat.estado = 0;

        CP3->battery->estado = 0;
        CP3->battery->set_soc(0);
        CP3->battery->set_uid(0);
    }
    if(CP_number == 4){
        bat.set_soc(CP4->battery->get_soc());
        bat.set_uid(CP4->battery->get_uid());
        bat.estado = 0;

        CP4->battery->estado = 0;
        CP4->battery->set_soc(0);
        CP4->battery->set_uid(0);
    }
    if(CP_number == 5){
        bat.set_soc(CP5->battery->get_soc());
        bat.set_uid(CP5->battery->get_uid());
        bat.estado = 0;

        CP5->battery->estado = 0;
        CP5->battery->set_soc(0);
        CP5->battery->set_uid(0);
    }
    if(CP_number == 6){
        bat.set_soc(CP6->battery->get_soc());
        bat.set_uid(CP6->battery->get_uid());
        bat.estado = 0;

        CP6->battery->estado = 0;
        CP6->battery->set_soc(0);
        CP6->battery->set_uid(0);
    }
    if(CP_number == 7){
        bat.set_soc(CP7->battery->get_soc());
        bat.set_uid(CP7->battery->get_uid());
        bat.estado = 0;

        CP7->battery->estado = 0;
        CP7->battery->set_soc(0);
        CP7->battery->set_uid(0);
    }
    return bat;
}

int ETB::baterias_presentes(){
    int i = 0;
    if(CP1->battery->estado == 1){
        i++;
    }

    return i;
}

int ETB::baterias_carregando(){
    int i = 0;
    if(CP1->estado == 1){
        i++;
    }

    return i;
}

float ETB::tempo_carregamento(int CP_number){
    float time;
    if(CP1->estado == 1){
        time = (100 - CP1->battery->get_soc())/0.05;
    }
    if(CP2->estado == 1){
        time = (100 - CP2->battery->get_soc())/0.05;
    }
    if(CP3->estado == 1){
        time = (100 - CP3->battery->get_soc())/0.05;
    }
    if(CP4->estado == 1){
        time = (100 - CP4->battery->get_soc())/0.05;
    }
    if(CP5->estado == 1){
        time = (100 - CP5->battery->get_soc())/0.05;
    }
    if(CP6->estado == 1){
        time = (100 - CP6->battery->get_soc())/0.05;
    }
    if(CP7->estado == 1){
        time = (100 - CP7->battery->get_soc())/0.05;
    } 
    return time;
}

CP::CP(long long int uid, float soc, int bat_estado){
    this->estado = 0;
    battery = new Bateria(uid, soc, 1, bat_estado);
}






