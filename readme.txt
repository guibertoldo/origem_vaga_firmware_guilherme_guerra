	Todas as funcionalidade propostas foram implementadas, i.e.:
-Ligar e desligar a moto;
-Associar ou liberar a bateria da moto;
-Acelerar ou freiar a moto;
-Associar ou liberar uma bateria da ETB;
-Carregar a bateria na ETB quando essa estiver com menos de 100%;
-Obter o numero de baterias presentes e carregando numa ETB;
-Tempo em segundos até a conclusão da carga de uma bateria no ETB;
-Simulação dos 30min nas condições propostas.

	Em relação as implemenações diferentes do que foi proposto, tem-se:
-A bateria como atributo de cada CP;
-Dois metodos criados para simular a perda de carga da bateria quando a moto estiver ligada ou parada;
-A simulação de 60 segundos após a chegada.


	Para compilar e executar o programa, os seguintes passos devem ser seguidos:
1)Abra o terminal do linux.
2)Acesse o diretorio onde foram salvos os arquivos.
3)Digite o comando "make".
4)Execute o programa com "./main".


	Em relação ao ambiente em que esse projeto foi denvolvimento:
-Ubuntu 20.04.3 LTS rodado em uma maquina virtual;
-C++11
	
