CC = g++
CFLAGS = -Wall -g --std=c++11
 
main: main.o Dominios.o
	$(CC) $(CFLAGS) -o main main.o Dominios.o
 
main.o: main.cpp Dominios.h
	$(CC) $(CFLAGS) -c main.cpp
 
Dominios.o: Dominios.h
 
